define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),

        TvChannel = Backbone.Model.extend({
            urlRoot: "http://examontv.com/sd/index.php/api/Rest/channels/",
            parse: function(response) {
                if(typeof response.data != 'undefined')
                    return response.data.tvchannels;
                else
                    return response;
            }
        }),

        TvChannels = Backbone.Collection.extend({
            model: TvChannel,
            url: "http://examontv.com/sd/index.php/api/Rest/channels",
            parse: function(response) {
                return response.data.tvchannels;
            }
        }),

        TvShow = Backbone.Model.extend({
            urlRoot: "http://examontv.com/sd/index.php/api/Rest/getCurrShow",
            parse: function(response) {
                if(typeof response.data.tvchannels != 'undefined')
                    return response.data.tvchannels;
                else if(typeof response.data.tvshows != 'undefined')
                    return response.data.tvshows;                    
            }
        }),

        Tweet = Backbone.Model.extend({
            urlRoot: "http://examontv.com/sd/index.php/api/Rest/liveTweets",
            parse: function(response) {
                if(typeof response.data != 'undefined')
                    return response.data.tweetStore;
                else
                    return response;               
            }
        }),

        Tweets = Backbone.Collection.extend({
            model: Tweet,
            url: "http://examontv.com/sd/index.php/api/Rest/liveTweets",
            parse: function(response) {
                return response.data.tweetStore;                  
            }
        }),



        HsTweet = Backbone.Model.extend({
            urlRoot: "http://106.187.54.254/tweet-store/index.php/api/TweetsUnique/search",
            parse: function(response) {
                if(typeof response.data != 'undefined')
                    return response.data.tweets;  
                else
                    return response;     
                             
            }
        }),

        HsTweets = Backbone.Model.extend({
            url: "http://106.187.54.254/tweet-store/index.php/api/TweetsUnique/search",
            model: HsTweet,
            parse: function(response) {
                return response.data.tweets;               
            }
        });

    return {
        TvChannel: TvChannel,
        TvChannelCollection: TvChannels,
        TvShow: TvShow,
        Tweet:Tweet,
        TweetCollection: Tweets,
        HsTweet: HsTweet
    };

});