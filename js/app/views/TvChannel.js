define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/TvChannel.html'),

        template = _.template(tpl);

    return Backbone.View.extend({

        initialize: function (options) {
            _.bindAll(this, 'render');
            this.tvChannel = options.tvChannel;
            this.tvShow = options.tvShow;
            this.tweets = options.tweets;
            console.log(this.tvShow);
            this.render();
            this.tvChannel.on('change', this.render);
            this.tvShow.on('change', this.render);
            this.tweets.on('change', this.render);
        },

        render: function () {
            this.$el.html(template(this));
            return this;
        }
    });
});