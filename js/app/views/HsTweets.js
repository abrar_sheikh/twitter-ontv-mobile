define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/HsTweets.html'),

        template = _.template(tpl);

    return Backbone.View.extend({

        initialize: function (options) {
            _.bindAll(this, 'render');
            this.tweets = options.tweets;
            this.render();
            this.tweets.on('change', this.render);
        },

        render: function () {
            this.$el.html(template(this));
            return this;
        }
    });
});