define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/TvChannelList.html'),

        template = _.template(tpl);

    return Backbone.View.extend({

        initialize: function () {
            this.render();
            this.collection.on("reset", this.render, this);
        },

        render: function () {
            var imgMap = {'17': 'images/channel/starplus.png',
                "19": "images/channel/colors.png",
                "29": "images/channel/aajtak.png",
                "20": "images/channel/sony.png",
                "22": "images/channel/mtv.png",
                "21": "images/channel/v.png",
                "24": "images/channel/times_now.png",
                "25": "images/channel/star_sports1.png",
                "26": "images/channel/star_sports2.png",
                "28": "images/channel/star_sports4.png",
                "18": "images/channel/star_world.png"};
            this.$el.html(template({tvChannels: this.collection.toJSON(), imgMap:imgMap}));
            return this;
        }

    });

});