define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        TvChannelListView    = require('app/views/TvChannelList'),
        models              = require('app/models/TvChannel'),
        tpl                 = require('text!tpl/Home.html'),

        template = _.template(tpl);


    return Backbone.View.extend({

        initialize: function () {
            this.tvChannelList = new models.TvChannelCollection();
            this.tvChannelList.fetch({reset: true});
            this.render();
        },

        render: function () {
            this.$el.html(template());
            this.listView = new TvChannelListView({collection: this.tvChannelList, el: $(".content-1", this.el)});
            return this;
        },

        events: {
            "keyup .search-key":    "search",
            "keypress .search-key": "onkeypress"
        },

        search: function (event) {
            var key = $('.search-key').val();
            this.tvChannelList.fetch({reset: true, data: {name: key}});
        },

        onkeypress: function (event) {
            if (event.keyCode === 13) { // enter key pressed
                event.preventDefault();
            }
        }

    });

});