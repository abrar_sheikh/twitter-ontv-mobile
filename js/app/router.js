define(function (require) {

    "use strict";

    var $           = require('jquery'),
        Backbone    = require('backbone'),
        PageSlider  = require('app/utils/pageslider'),
        HomeView    = require('app/views/Home'),

        slider = new PageSlider($('body')),

        homeView = new HomeView();

    return Backbone.Router.extend({

        routes: {
            "": "home",
            "tvChannel/:id": "channelDetails",
            "hsTweets/:id": "hsTweetList"
        },

        home: function () {
            homeView.delegateEvents();
            slider.slidePage(homeView.$el);
        },

        channelDetails: function (id) {
            require(["app/models/TvChannel", "app/views/TvChannel"], function (models, TvChannelView) {
                var successFunction = function () {
                    slider.slidePage(new TvChannelView({tvChannel: tvChannel, tvShow:tvShow, tweets:tweets}).$el);
                };
                var errorFunction = function () {
                    console.log('error');
                };
                var tvChannel = new models.TvChannel({id: id});
                var tvShow = new models.TvShow({id: id});
                var tweets = new models.Tweet({id: id});

                $.when(tvChannel.fetch(), tvShow.fetch(), tweets.fetch()).then(successFunction, errorFunction);

            });
        },

        hsTweetList: function (id) {
            require(["app/models/TvChannel", "app/views/HsTweets"], function (models, HsTweetslView) {
                var successFunction = function () {
                    console.log(tweets);
                    slider.slidePage(new HsTweetslView({tweets:tweets}).$el);
                };
                var errorFunction = function () {
                    console.log('error');
                };
                id = encodeURI(id);
                var tweets = new models.HsTweet({id: id});

                $.when(tweets.fetch()).then(successFunction, errorFunction);

            });
        }
    });

});